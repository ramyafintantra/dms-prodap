﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Data;
using DMS.Model;

namespace DMS.Data
{
    public class DepartmentMaster_Data
    {
        MySqlConnection Con = new MySqlConnection(ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString);
        //read the data
        public List<DepartmentMaster_Model> deptmstdetail()
        {
            try
            {
                List<DepartmentMaster_Model> DepartmentList = new List<DepartmentMaster_Model>();
                DataTable dt = new DataTable();
                MySqlCommand cmd = new MySqlCommand("SP_GetAllDepartmentDetails", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                Con.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(dt);
                Con.Close();
                foreach (DataRow dr in dt.Rows)
                {
                    DepartmentList.Add
                        (
                        new DepartmentMaster_Model
                        {
                            DepartmentId = Convert.ToInt32(dr["Dept_Id"].ToString()),
                            DepartmentName = dr["Dept_Name"].ToString(),
                            CreatedDate = Convert.ToDateTime(dr["Dept_Created_Date"].ToString()),
                            //Grade = Convert.ToInt32(dr["Grade"].ToString()),
                            Createdby = dr["Dept_Created_By"].ToString(),

                        });
                }
                return DepartmentList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //insert the data
        public DepartmentMaster_Model DptMstSave(DepartmentMaster_Model Deptmodel)
        {
            try
            {
                DataTable dt = new DataTable();
                MySqlCommand cmd = new MySqlCommand("SP_DepartmentSaveUpdateDelete", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("In_Action", MySqlDbType.VarChar).Value = "Insert";
                cmd.Parameters.Add("In_Dept_Id", MySqlDbType.Int32).Value = "0";
                cmd.Parameters.Add("In_Dept_Name", MySqlDbType.VarChar).Value = Deptmodel.DepartmentName;
                cmd.Parameters.Add("In_UserID", MySqlDbType.Int32).Value = Deptmodel.Createdby;
                Con.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(dt);
                Con.Close();            
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Deptmodel;
        }

        //update the data
        public DepartmentMaster_Model DptMstUpdate(DepartmentMaster_Model Deptmodel)
        {
            try
            {
                DataTable dt = new DataTable();
                MySqlCommand cmd = new MySqlCommand("SP_DepartmentSaveUpdateDelete", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("In_Action", MySqlDbType.VarChar).Value = "Update";
                cmd.Parameters.Add("In_Dept_Id", MySqlDbType.Int32).Value = Deptmodel.DepartmentId;
                cmd.Parameters.Add("In_Dept_Name", MySqlDbType.VarChar).Value = Deptmodel.DepartmentName;
                cmd.Parameters.Add("In_UserID", MySqlDbType.Int32).Value = Deptmodel.Createdby;
                Con.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(dt);
                Con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Deptmodel;
        }

        //delete the data
        public DataTable DeletingDepartment(int? DeptID)
        {
            DataTable dt = new DataTable();
            try
            {
                MySqlCommand cmd = new MySqlCommand("SP_DepartmentSaveUpdateDelete", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("In_Action", MySqlDbType.VarChar).Value = "Delete";
                cmd.Parameters.Add("In_Dept_Id", MySqlDbType.Int32).Value = DeptID;
                cmd.Parameters.Add("In_Dept_Name", MySqlDbType.VarChar).Value = "0";
                cmd.Parameters.Add("In_UserID", MySqlDbType.Int32).Value = "0";
                Con.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(dt);
                Con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}
