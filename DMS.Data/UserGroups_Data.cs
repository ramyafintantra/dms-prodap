﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DMS.Data
{
    public class UserGroups_Data
    {
        MySqlConnection Con = new MySqlConnection(ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString);
        public DataTable GetUserGroups()
        {
            DataTable tab = new DataTable();
            try
            {
                DataTable dt = new DataTable();
                MySqlCommand cmd = new MySqlCommand("SP_GetUserGroups", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add("In_Action", MySqlDbType.VarChar).Value = CommonVal;
                Con.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(dt);
                Con.Close();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetTreeview(int usergroup_gid)
        {
            DataTable tab = new DataTable();
            try
            {
                DataTable dt = new DataTable();
                MySqlCommand cmd = new MySqlCommand("SP_GetTreeView", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("in_usergroup_gid", MySqlDbType.VarChar).Value = usergroup_gid;
                Con.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(dt);
                Con.Close();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string[] CreateUserGroups(int usergroup_gid, string usergroup_name, string action_by)
        {
            string[] result = { };
            DataTable tab = new DataTable();
            //DataConnection con = new DataConnection();
            try
            {
                Dictionary<string, Object> values = new Dictionary<string, object>();
                values.Add("in_usergroup_gid", usergroup_gid);
                values.Add("in_usergroup_name", usergroup_name);
                values.Add("in_action_by", action_by);
                values.Add("out_usergroup_gid", "out");
                values.Add("out_msg", "out");
                values.Add("out_result", "out");
                result = RunDmlProcArrayList("SP_SetUserGroup", values);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string[] RunDmlProcArrayList(string command, Dictionary<string, Object> values = null)
        {
            int rowsChanged = 0;
            MySqlCommand cmd = new MySqlCommand(command,Con);
           // cmd.CommandText = command;
            cmd.CommandType = CommandType.StoredProcedure;
            string retmsg = string.Empty;
            string retresult = string.Empty;
            string retid = string.Empty;
            try
            {
                if (values != null)
                {
                    foreach (string key in values.Keys)
                    {
                        if (values[key] == "out")
                        {
                            cmd.Parameters.Add(key, MySqlDbType.VarChar);
                            cmd.Parameters[key].Direction = ParameterDirection.Output;

                        }
                        else
                        {
                            cmd.Parameters.AddWithValue(key, values[key]);
                        }
                    }
                }
                Con.Open();
                rowsChanged = cmd.ExecuteNonQuery();
                retmsg = (string)cmd.Parameters["out_msg"].Value;
                retresult = (string)cmd.Parameters["out_result"].Value;
                if (cmd.Parameters["out_usergroup_gid"].Value.ToString() != "")
                {
                    retid = (string)cmd.Parameters["out_usergroup_gid"].Value;
                }
                else
                {
                    retid = "0";
                }
                string[] returnvalues = { retmsg, retresult, retid };
                Con.Close();
                return returnvalues;
            }
            catch (Exception ex)
            {
                Con.Close();
                string[] returnvalues = { };
                return returnvalues;

            }
        }
        public string[] SetRightsFlag(int menu_gid, int usergroup_gid, int rights_flag)
        {
            string[] result = { };
            DataTable tab = new DataTable();
            try
            {
                Dictionary<string, Object> values = new Dictionary<string, object>();
                values.Add("in_menu_gid", menu_gid);
                values.Add("in_usergroup_gid", usergroup_gid);
                values.Add("in_rights_flag", rights_flag);
                values.Add("out_msg", "out");
                values.Add("out_result", "out");
                result = RunDmlProcArray("SP_InsertRights", values);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string[] DeleteUserGroups(int? usergroup_gid)
        {
            string[] result = { };
            DataTable tab = new DataTable();
            try
            {
                Dictionary<string, Object> values = new Dictionary<string, object>();
                values.Add("in_usergroup_gid", usergroup_gid);
                values.Add("out_msg", "out");
                result = RunDelProcArray("SP_Deleteusergroup", values);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string[] RunDelProcArray(string command, Dictionary<string, Object> values = null)
        {
            int rowsChanged = 0;
            MySqlCommand cmd = new MySqlCommand(command, Con);
            cmd.CommandType = CommandType.StoredProcedure;
            string retmsg = string.Empty;
            try
            {
                if (values != null)
                {
                    foreach (string key in values.Keys)
                    {
                        if (values[key] == "out")
                        {
                            cmd.Parameters.Add(key, MySqlDbType.VarChar);
                            cmd.Parameters[key].Direction = ParameterDirection.Output;

                        }
                        else
                        {
                            cmd.Parameters.AddWithValue(key, values[key]);
                        }
                    }
                }
                Con.Open();
                rowsChanged = cmd.ExecuteNonQuery();
                retmsg = (string)cmd.Parameters["out_msg"].Value;
                string[] returnvalues = { retmsg};
                Con.Close();
                return returnvalues;
            }
            catch (Exception ex)
            {
                string[] returnvalues = { };
                return returnvalues;
            }
        }


        public string[] RunDmlProcArray(string command, Dictionary<string, Object> values = null)
        {
            int rowsChanged = 0;
            MySqlCommand cmd = new MySqlCommand(command,Con);
            cmd.CommandType = CommandType.StoredProcedure;
            string retmsg = string.Empty;
            string retresult = string.Empty;
            try
            {
                if (values != null)
                {
                    foreach (string key in values.Keys)
                    {
                        if (values[key] == "out")
                        {
                            cmd.Parameters.Add(key, MySqlDbType.VarChar);
                            cmd.Parameters[key].Direction = ParameterDirection.Output;

                        }
                        else
                        {
                            cmd.Parameters.AddWithValue(key, values[key]);
                        }
                    }
                }
                Con.Open();
                rowsChanged = cmd.ExecuteNonQuery();
                retmsg = (string)cmd.Parameters["out_msg"].Value;
                retresult = (string)cmd.Parameters["out_result"].Value;
                string[] returnvalues = { retmsg, retresult };
                Con.Close();
                return returnvalues;
            }
            catch (Exception ex)
            {
                string[] returnvalues = { };
                return returnvalues;
            }
        }

        public DataTable GetMenu(int userID)
        {
            DataTable tab = new DataTable();
            try
            {
                MySqlCommand cmd = new MySqlCommand("SP_Getmenu", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("in_usergroup_gid", MySqlDbType.Int32).Value = userID;
                Con.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(tab);
                Con.Close();
                return tab;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
