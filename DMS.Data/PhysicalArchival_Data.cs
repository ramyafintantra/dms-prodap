﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;
using DMS.Model;

namespace DMS.Data
{
    public class PhysicalArchival_Data
    {
        MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString);
        public DataSet GetIndexedDocuments(int? DeptID1, int? Unit1, int? Dgroup1, int? Dname1)
        {
            try
            {
                DataSet ds = new DataSet();
                MySqlCommand cmd = new MySqlCommand("SP_Getstorageattribforgrid", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("In_DeptID", MySqlDbType.Int32).Value = DeptID1;
                cmd.Parameters.Add("In_UnitID", MySqlDbType.Int32).Value = Unit1;
                cmd.Parameters.Add("In_DgroupID", MySqlDbType.Int32).Value = Dgroup1;
                cmd.Parameters.Add("In_DnameID", MySqlDbType.Int32).Value = Dname1;
                cmd.Parameters.Add("In_Dynamicfields", MySqlDbType.VarChar).Value = '0';
                con.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet Initialvalues(int? deptid, int? unit, int? docgroup, int? docname, string Aaction, int? attribgid)
        {
            try
            {
                DataSet ds = new DataSet();
                MySqlCommand cmd = new MySqlCommand("SP_GetdynamicStorageattributes", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("In_DeptID", MySqlDbType.Int32).Value = deptid;
                cmd.Parameters.Add("In_UnitID", MySqlDbType.Int32).Value = unit;
                cmd.Parameters.Add("In_DgroupID", MySqlDbType.Int32).Value = docgroup;
                cmd.Parameters.Add("In_DnameID", MySqlDbType.Int32).Value = docname;
                cmd.Parameters.Add("In_Action", MySqlDbType.VarChar).Value = Aaction;
                cmd.Parameters.Add("In_Attribgid", MySqlDbType.Int32).Value = attribgid;
                con.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveStorageAttributes(PhysicalArchival_Model modelObj, List<PhysicalArchival_Model> ModelObjList)
        {
            try
            {

                int Result = 0;
                con.Open();
                for (int i = 0; i < ModelObjList.Count; i++)
                {
                    MySqlCommand cmd = new MySqlCommand("SP_Savephysicalattributes", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("In_AttribId", MySqlDbType.Int32).Value = ModelObjList[i].Atrgid;
                    cmd.Parameters.Add("In_SatrName", MySqlDbType.VarChar).Value = ModelObjList[i].Satrname;
                    cmd.Parameters.Add("In_SatrLength", MySqlDbType.VarChar).Value = ModelObjList[i].Satrlen;
                    cmd.Parameters.Add("In_SatrType", MySqlDbType.VarChar).Value = ModelObjList[i].Satrtype;
                    cmd.Parameters.Add("In_SatrMandotry", MySqlDbType.VarChar).Value = ModelObjList[i].SatrMand;
                    cmd.Parameters.Add("In_SatrDesc", MySqlDbType.VarChar).Value = ModelObjList[i].DynamicVal;
                    cmd.Parameters.Add("In_UserId", MySqlDbType.Int32).Value = modelObj.UserId;
                    cmd.Parameters.Add("In_Action", MySqlDbType.VarChar).Value = modelObj.action;
                    Result = cmd.ExecuteNonQuery(); 
                }                            
                con.Close();
                return Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable checkvalidone(int? GridID)
        {
            try
            {
                DataTable dt = new DataTable();
                MySqlCommand cmd = new MySqlCommand("SP_checkvalidone", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("In_AttribID", MySqlDbType.Int32).Value = GridID;
                con.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(dt);
                con.Close();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
