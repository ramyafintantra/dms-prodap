﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.IO;
using DMS.Model;
using DMS.Service;
using DMS.Web.Filters;
using System.Configuration;
using System.Data.OleDb;


namespace DMS.Web.Controllers
{
    [UserAuntheication]
    public class DocArchivalController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(DocArchivalController));  //Declaring Log4Net        
        DocArchival_Model ModelObj = new DocArchival_Model();  //Model Object
        DocArchival_Service ServiceObj = new DocArchival_Service();  //Service Object

        // GET: DocArchival
        [HttpGet]
        public ActionResult DocArchival_Multiple()
        {
            Dept_Union_Cat_SubCat_Model deptsModel = new Dept_Union_Cat_SubCat_Model();
            Session["fileid"] = 0;
            return View(deptsModel);
        }

        /****** Multiple Document Archival/Uploading Controller Method - START ******/
        [HttpPost]
        //public ActionResult DocArchival_Multiple(HttpPostedFileBase[] FileUpload1, Dept_Union_Cat_SubCat_Model DCObj)
        //{
        public ActionResult Save_Multiple_File(int department, int unit, int docgroup, int docname, string useracceptance)
        {
            string message = "";
            Int64 nooffiles = 0;
            string checkfile = "";
            int Result;
            try
            {
                HttpPostedFileBase[] FileUpload1 = Session["Multiple"] as HttpPostedFileBase[];
                if (FileUpload1 != null)
                {
                    ModelObj.DeptId = department;
                    ModelObj.UnitId = unit;
                    ModelObj.CatgId = docgroup;
                    ModelObj.SubCatgId = docname;
                    ModelObj.UserId = Convert.ToInt32(Session["Emp_Id"].ToString());
                    if (useracceptance == "allow")
                    {
                        //Passing all values to Service class through Model object list.
                        Result = ServiceObj.SaveDocInfo(ModelObj, FileUpload1);
                        if (Result == 1)
                        {
                            message = "inserted";
                            nooffiles = FileUpload1.Count();
                            //ViewBag.Message = FileUpload1.Count() + " " + "Files Uploaded Scuccessfully!....";
                        }
                    }
                    else
                    {
                        checkfile = ServiceObj.checkfilemultiple(ModelObj, FileUpload1);
                        if (checkfile == "exist")
                        {
                            message = "exist";
                        }
                        else
                        {
                            //Passing all values to Service class through Model object list.
                            Result = ServiceObj.SaveDocInfo(ModelObj, FileUpload1);
                            if (Result == 1)
                            {
                                message = "inserted";
                                nooffiles = FileUpload1.Count();
                            }
                        }
                    }
                }
                return Json(new {message,nooffiles}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return Json(message, JsonRequestBehavior.AllowGet);
            }
            //Dept_Union_Cat_SubCat_Model deptsModel = new Dept_Union_Cat_SubCat_Model();
            //return View(deptsModel);
        }
        /****** Multiple Document Archival/Uploading Controller Method - END ******/

        /****** Single Document Archival/Uploading Controller Method - START ******/
        [HttpPost]
        public ActionResult Save_Single_File(int department, int unit, int docgroup, int docname, string action, int fileid, string useracceptance)
        {
            string message = "";
            string checkfile = "";
            int result;
            try
            {
                string ActionMode = action;
                Int64? FileId = fileid;
                ModelObj.UserId = Convert.ToInt32(Session["Emp_Id"].ToString());
                ModelObj.DeptId = department;
                ModelObj.UnitId = unit;
                ModelObj.CatgId = docgroup;
                ModelObj.SubCatgId = docname;
                ModelObj.FileID = fileid;
                HttpPostedFileBase file = Session["Single"] as HttpPostedFileBase;
                if (ActionMode == "I")
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        if (useracceptance == "allow")
                        {
                            //Passing all values to Service class through Model object list.
                            result = ServiceObj.SaveSingleFile(ModelObj, file);
                            if (result == 1)
                            {
                                message = "inserted";
                                //ViewBag.Message = "File uploaded successfully..!!";
                            }
                        }
                        else
                        {
                            checkfile = ServiceObj.checkfile(ModelObj, file);
                        }
                        if (checkfile == "exist")
                        {
                            message = "exist";
                        }
                        else
                        {
                            //Passing all values to Service class through Model object list.
                            result = ServiceObj.SaveSingleFile(ModelObj, file);
                            if (result == 1)
                            {
                                message = "inserted";
                            }
                        }
                    }
                }

                if (ActionMode == "E")
                {
                     result = 0;
                    if (file != null && file.ContentLength > 0)
                    {
                        //Passing all values to Service class through Model object list.
                        result = ServiceObj.EditScannedDocwithfile(ModelObj, file);
                    }
                    else
                    {
                        result = ServiceObj.EditScannedDocwithoutfile(ModelObj);
                    }
                    if (result == 1)
                    {
                        message = "edited";
                    }
                }

                if (ActionMode == "D")
                {
                    if (FileId != null)
                    {
                        DataTable dt1 = new DataTable();
                        dt1 = ServiceObj.DeleteScannedFile(FileId, ActionMode, ModelObj);
                        if (dt1.Rows.Count > 0)
                        {
                            string fullPath = dt1.Rows[0]["DOC_Arch_FilePath"].ToString();
                            if (System.IO.File.Exists(fullPath))
                            {
                                System.IO.File.Delete(fullPath);
                                //ViewBag.Message = "File Deleted successfully..!!";
                            }
                            message = "deleted";

                        }
                    }
                }
                return Json(message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return Json(message, JsonRequestBehavior.AllowGet);
            }
            //return View(deptsModel);
        }
        /****** Single Document Archival/Uploading Controller Method - END ******/

        [HttpGet]
        public ActionResult DocArchival_Single(int? id, string mode)
        {
            Dept_Union_Cat_SubCat_Model deptsModel = new Dept_Union_Cat_SubCat_Model();
            Session["fileid"] = null;
            if (id != null)
            {
                Session["fileid"] = id;
                deptsModel.ActionMode = mode;
                deptsModel.FileId = id;
            }
            else
            {
                deptsModel.ActionMode = "I";
                Session["fileid"] = 0;
                deptsModel.FileId = 0;
            }
            return View(deptsModel);
        }

        [HttpPost]
        public void SingleFile_onchange(HttpPostedFileBase file)
        {
            Session["Single"] = null;
            Session["Single"] = file;
        }

        [HttpPost]
        public void MultipleFile_onchange()
        {
            HttpFileCollectionBase files = Request.Files;
            HttpPostedFileBase[] multiplefiles = new HttpPostedFileBase[files.Count];   
            for (int i = 0; i < files.Count; i++)
            {
                multiplefiles[i] = files[i];
            }
            Session["Multiple"] = null;
            Session["Multiple"] = multiplefiles;
        }

        //Json method for fetching values in databsae to all dropdownlist And passing list values to view through jsonresult.
        public JsonResult DepartmentEdit(string type, string actiontype)
        {
            int? DocID = Convert.ToInt32(Session["fileid"].ToString());
            List<Dep_union_dropdown> dept = new List<Dep_union_dropdown>();
            List<Dep_union_dropdown> Select = new List<Dep_union_dropdown>();
            try
            {
                dept = ServiceObj.GeALL(type, actiontype);
                if (DocID != null && DocID != 0)
                {
                    Select = ServiceObj.GetselectedItem(DocID, type);
                }
                else
                {
                    Select = null;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(new { dept, Select }, JsonRequestBehavior.AllowGet);
        }

        //Json method for fetching values in databsae to unit,docgroup and docname dropdownlist using DepartmentId based.And passing list values to view through jsonresult. 
        public JsonResult GetDept(int DeptID)
        {
            List<Dep_union_dropdown> Get_unit = new List<Dep_union_dropdown>();
            List<Dep_union_dropdown> Get_docgroup = new List<Dep_union_dropdown>();
            List<Dep_union_dropdown> Get_docname = new List<Dep_union_dropdown>();
            try
            {
                Get_unit = ServiceObj.GetUnit(DeptID);
                Get_docgroup = ServiceObj.GetDocGroup(DeptID);
                Get_docname = ServiceObj.GetDocName(DeptID);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(new { Get_unit, Get_docgroup, Get_docname }, JsonRequestBehavior.AllowGet);
        }

        //Json method for fetching values in databsae to department,docgroup and docname dropdownlist using UnitId based.And passing list values to view through jsonresult. 
        public JsonResult GetUnit(int UnitID)
        {
            List<Dep_union_dropdown> Get_dept1 = new List<Dep_union_dropdown>();
            List<Dep_union_dropdown> Get_docgroup1 = new List<Dep_union_dropdown>();
            List<Dep_union_dropdown> Get_docname1 = new List<Dep_union_dropdown>();
            try
            {
                Get_dept1 = ServiceObj.GetDept1(UnitID);
                Get_docgroup1 = ServiceObj.GetDocGroup1(UnitID);
                Get_docname1 = ServiceObj.GetDocName1(UnitID);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(new { Get_dept1, Get_docgroup1, Get_docname1 }, JsonRequestBehavior.AllowGet);
        }

        //Json method for fetching values in databsae to department,Unit and docname dropdownlist using DocgroupId based.And passing list values to view through jsonresult. 
        public JsonResult GetDocGroup(int DocGroupID)
        {
            List<Dep_union_dropdown> Get_dept2 = new List<Dep_union_dropdown>();
            List<Dep_union_dropdown> Get_unit2 = new List<Dep_union_dropdown>();
            List<Dep_union_dropdown> Get_docname2 = new List<Dep_union_dropdown>();
            try
            {
                Get_dept2 = ServiceObj.GetDept2(DocGroupID);
                Get_unit2 = ServiceObj.GetUnit2(DocGroupID);
                Get_docname2 = ServiceObj.GetDocName2(DocGroupID);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(new { Get_dept2, Get_unit2, Get_docname2 }, JsonRequestBehavior.AllowGet);
        }

        //Json method for fetching values in databsae to department,Unit and docgroup dropdownlist using DocnameId based.And passing list values to view through jsonresult. 
        public JsonResult GetDocName(int DocNameId)
        {
            List<Dep_union_dropdown> Get_dept3 = new List<Dep_union_dropdown>();
            List<Dep_union_dropdown> Get_unit3 = new List<Dep_union_dropdown>();
            List<Dep_union_dropdown> Get_docgroup3 = new List<Dep_union_dropdown>();
            try
            {
                Get_dept3 = ServiceObj.GetDept3(DocNameId);
                Get_unit3 = ServiceObj.GetUnit3(DocNameId);
                Get_docgroup3 = ServiceObj.GetDocGroup3(DocNameId);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(new { Get_dept3, Get_unit3, Get_docgroup3 }, JsonRequestBehavior.AllowGet);
        }

    }
}