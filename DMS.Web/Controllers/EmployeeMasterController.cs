﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using DMS.Web.Filters;
using DMS.Model;
using DMS.Service;
using System.Data;

namespace DMS.Web.Controllers
{
    [UserAuntheication]
    public class EmployeeMasterController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(DocumentLinkingController));  //Declaring Log4Net 
        EmployeeMaster_Model EDtlObj = new EmployeeMaster_Model(); //Creating model object.
        EmployeeMaster__Service EmpSrvObj = new EmployeeMaster__Service();//Creating service object.

        public ActionResult EmployeeMaster()// GET: EmployeeMaster.
        {
            return View();
        }
        //Employee Master read.
        public ActionResult EmployeeGrid_Read([DataSourceRequest]DataSourceRequest request) 
        {
            try
            {
                return Json(EmpSrvObj.EmployeeMstDtl().ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        }
        //Save employee details. Bug_Id-7 Dateformat issue fixed.
        public ActionResult SaveEmployee([DataSourceRequest] DataSourceRequest request, EmployeeMaster_Model EDtlObj) 
        {
            try
            {
                EDtlObj.UserID = Convert.ToInt32(Session["Emp_Id"].ToString());
                return Json(EmpSrvObj.SaveEmployee(EDtlObj));

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
        }
        private DateTime GetUtcTime(DateTime dateTime, decimal dateTimeOffsetMinutes)
        {
            return DateTime.SpecifyKind(dateTime.AddMinutes((double)dateTimeOffsetMinutes), DateTimeKind.Utc);
        }
        //Update employee details.
        public ActionResult EmployeeGrid_Update([DataSourceRequest] DataSourceRequest request, EmployeeMaster_Model EDtlObj)
        {
            try
            {
                DataTable dt = new DataTable();
                EDtlObj.UserID = Convert.ToInt32(Session["Emp_Id"].ToString());
                return Json(EmpSrvObj.EmployeeMstDtlUpdate(EDtlObj));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }      
        }

        //delete the data.
        public ActionResult DeletingEmployee(int? EmployeeID)
        {
            DataTable dt = new DataTable();
            string Result = "";
            try
            {
                dt = EmpSrvObj.DeletingDepartment(EmployeeID);
                if (dt.Rows.Count > 0)
                {
                    Result = dt.Rows[0][0].ToString();
                }
                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }
        //binding popup dropdown.
        public JsonResult GetEmployeeNames(string CommonVal) 
        {
            List<EmployeeMaster_Model> Get_Dept = new List<EmployeeMaster_Model>();
            try
            {
                Get_Dept = EmpSrvObj.GetDepartment(CommonVal);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Get_Dept, JsonRequestBehavior.AllowGet);
        }
    }
}