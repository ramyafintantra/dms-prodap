﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Model
{
    public class PhysicalArchival_Model
    {
        public string Satrname { get; set; }
        public string Satrid { get; set; }
        public string Satrtype { get; set; }
        public string Satrlen { get; set; }
        public string SatrMand { get; set; }
        public string Satrdesc { get; set; }
        public string Satrmode { get; set; }
        public PhysicalArchival_Model()
        {
            dept = new List<PhysicalArchival_Model>();
        }
        public List<PhysicalArchival_Model> dept { get; set; }
        public int UserId { get; set; }
        public int SatrCount { get; set; }
        public string DynamicVal { get; set; }
        public int Atrgid { get; set; }
        public string action { get; set; }
    }
}
